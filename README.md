# Jenkins & Jmeter

How to setup locally see [local setup](local_setup/local_setup.md).

[[_TOC_]]

## Jenkins pipeline

### 1. Simple

```groovy
pipeline {
    agent any
    stages {
        stage('Hello') {
            steps {
                echo "Hello!"
            }
        }
    }
}
```

### 2. Params

```groovy
pipeline {
    agent any
    parameters {
        string(name: 'username', defaultValue: 'nobody', description: 'Enter your name')
        choice(name: 'city', choices: ['Moscow', 'London'], description: 'Enter your city')
    }
    stages {
        stage('Hello') {
            steps {
                echo "Hello ${params.username} from ${params.city}!"
            }
        }
    }
}
```

### 3. Handlers & script block

```groovy
pipeline {
    agent any
    parameters {
        booleanParam(name: 'fail', defaultValue: false, description: 'Should I fail this build?')
    }
    stages {
        stage('Test script') {
            steps {
                echo "Should this job break? ${params.fail}"
                script {
                    if (params.fail) {
                    	print "Abort this build!"
                        currentBuild.result = 'ABORTED'
                        error('Job was aborted!')
                    } else {
                        echo "Success!"
                    }
                }
            }
            post {
                always {
                    echo 'Excecute anyway'
                }
            }
        }
    }
    post {
        success {
            echo "Pipeline succeeded because you passed fail = ${params.fail}"
        }
        aborted {
            echo "Pipeline aborted because you passed fail = ${params.fail}"
        }
    }
}
```


## Jenkins & Jmeter pipeline

```groovy
pipeline {
    agent any
    parameters {
        string(name: 'THREADS', defaultValue: '1', description: 'Number of threads to run')
    }
    stages {
        stage("Get testplan") {
            steps {
                git branch: 'main',
                        credentialsId: 'gitlab-pwd',
                        url: 'https://gitlab.com/otus-performance/ci-cd/lecture-1.git'
            }
        }
        stage("Check content") {
            steps {
                sh 'ls -lah'
            }
        }
        stage('Jmeter test') {
            steps {
                sh 'echo "computer_1\ncomputer_2\ncomputer_3" > data.csv'
                sh "jmeter -Jcsvfile=data.csv -Jthreads=$params.THREADS -n -t testplan.jmx -l result.csv"
            }
            post {
                always {
                    archiveArtifacts artifacts: 'jmeter.log'
                }
            }
        }
        stage('Generate report') {
            steps {
                perfReport 'result.csv'
            }
        }
    }
}
```

## Jenkins Shared Libraries

Example: [vars/runJmeter.groovy](vars/runJmeter.groovy)

Manage Jenkins -> Configure System -> Global Pipeline Libraries -> Add

```groovy
@Library('myLib@main') _
runJmeter('main', 'https://gitlab.com/otus-performance/ci-cd/lecture-1.git', 'testplan.jmx', 5, 'data.csv')
```

## References

- [Jenkins pipeline docs](https://www.jenkins.io/doc/book/pipeline/)
- [Jenkins Jmeter performance plugin](https://www.jenkins.io/doc/book/using/using-jmeter-with-jenkins/)
- [Jenkins in docker](https://hub.docker.com/r/jenkins/jenkins)
- [Jenkins shared library](https://www.jenkins.io/doc/book/pipeline/shared-libraries/)

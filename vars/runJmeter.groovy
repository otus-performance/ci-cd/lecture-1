def call(String branch, String gitUrl, String testPlan, int threads = 1, String testData) {
    pipeline {
        agent any
        stages {
            stage("Get testplan") {
                steps {
                    git branch: "${branch}",
                            url: "${gitUrl}"
                }
            }
            stage('Run JMeter') {
                steps {
                    sh 'echo "computer_1\ncomputer_2\ncomputer_3" > data.csv'
                    sh "jmeter -n -t ${testPlan} -l result.csv -Jthreads=${threads} -Jcsvfile=${testData}"
                }
                post {
                    always {
                        archiveArtifacts artifacts: 'result.csv'
                    }
                }
            }
            stage('Generate report') {
                steps {
                    perfReport 'result.csv'
                }
            }
        }
    }
}
